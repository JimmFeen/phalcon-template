<?php

namespace App\Handler;

use Phalcon\Http\Response;

class ErrorHandler extends AwareHandler
{
    public function handleNotFound()
    {
        $response = new Response();
        $response->setStatusCode(404, 'Not Found')
            ->setContent('Nothing to see here. Move along....')
            ->send();
    }

    public function handleException(\Exception $exception)
    {
        $this->getLogger()->error("Internal error : {$exception->getMessage()}");

        $response = new Response();
        $response
            ->setStatusCode(500, 'Internal error')
            ->setJsonContent(
                [
                    'code' => $exception->getCode(),
                    'status' => 'error',
                    'type' => get_class($exception),
                ], JSON_PRETTY_PRINT
            )
            ->send();
    }
}