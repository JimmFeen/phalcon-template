<?php


namespace App\Handler;


use Phalcon\Di\InjectionAwareInterface;
use Phalcon\DiInterface;
use Phalcon\Logger\Multiple;

class AwareHandler implements InjectionAwareInterface
{
    /**
     * @var DiInterface
     */
    protected $di;


    /**
     * Sets the dependency injector
     *
     * @param DiInterface $dependencyInjector
     */
    public function setDI(DiInterface $dependencyInjector)
    {
        $this->di = $dependencyInjector;
    }

    /**
     * Returns the internal dependency injector
     *
     * @return DiInterface
     */
    public function getDI()
    {
        return $this->di;
    }

    protected function getLogger(): Multiple
    {
        return $this->getDI()->get('logger');
    }
}