<?php

namespace App\Middleware;

use Phalcon\Http\Response;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use TweedeGolf\PrometheusClient\CollectorRegistry;

class ResponseMiddleware implements MiddlewareInterface
{
    /**
     * @var CollectorRegistry
     */
    private $collectorRegistry;

    /**
     * ResponseMiddleware constructor.
     * @param CollectorRegistry $collectorRegistry
     */
    public function __construct(CollectorRegistry $collectorRegistry)
    {
        $this->collectorRegistry = $collectorRegistry;
    }


    public function call(Micro $application)
    {
        $elapsed = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];

        $labels = [
            $application->request->getMethod(),
            $application->request->getURI(),
            $this->getResponse($application)->getStatusCode(),
        ];

        $this->collectorRegistry
            ->getCounter('requests')
            ->inc(1, $labels);

        if ($this->getResponse($application)->getStatusCode() === 200) {
            $this->collectorRegistry
                ->getHistogram('response_timing')
                ->observe(
                    $elapsed,
                    $labels
                );
        }
    }

    private function getResponse(Micro $application): Response
    {
        return $application->getDI()->get('response');
    }
}