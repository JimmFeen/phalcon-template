<?php

use Phalcon\Loader;
use Phalcon\Mvc\Micro;

define('BASE_PATH', dirname(__DIR__));

$loader = new Loader();

$loader->registerFiles(
    [
        BASE_PATH . '/vendor/autoload.php'
    ]
);

$loader->registerNamespaces(
    [
        "App\Model" => BASE_PATH . '/model/',
        "App\Controller" => BASE_PATH . '/controller/',
        "App\Handler" => BASE_PATH . '/handler/',
        "App\Middleware" => BASE_PATH . '/middleware/',
    ]
);

$loader->register();

$config = include BASE_PATH . "/config/config.php";
$di = include BASE_PATH . "/config/services.php";

$app = new Micro();
$app->setDi($di);

$routes = include BASE_PATH . "/config/routes.php";
foreach ($routes as $collectionClosure) {
    $app->mount($collectionClosure());
}

$app->notFound(
    function () use ($di) {
        $di->get("errorHandler")->handleNotFound();
    }
);

$app->error(
    function (\Exception $exception) use ($di) {
        $di->get("errorHandler")->handleException($exception);
    }
);

include BASE_PATH . "/config/events.php";

$app->handle();