<?php

namespace App\Controller;

use App\Model\MyObject;
use Phalcon\Mvc\Controller;

class MyObjectController extends Controller
{

    public function get(string $id)
    {
        $this->response->setStatusCode(200);
        $this->response->setJsonContent(MyObject::findFirst($id));
        $this->response->send();
    }
}
