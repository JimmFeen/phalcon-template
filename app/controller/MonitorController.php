<?php

namespace App\Controller;

use Phalcon\Mvc\Controller;
use TweedeGolf\PrometheusClient\CollectorRegistry;
use TweedeGolf\PrometheusClient\Format\TextFormatter;

class MonitorController extends Controller
{
    public function health()
    {
        $this->response->setStatusCode(200);
        $this->response->setJsonContent(
            [
                "status" => "UP",
            ]
        );
        $this->response->send();
    }

    public function metrics()
    {
        $formatter = new TextFormatter();
        $this->response->setStatusCode(200);
        $this->response->setContentType($formatter->getMimeType());
        $this->response->setContent($formatter->format($this->getCollectorRegistry()->collect()));
        $this->response->send();
    }

    private function getCollectorRegistry(): CollectorRegistry
    {
        return $this->getDI()->get('collectorRegistry');
    }
}