<?php

use Phalcon\Config;

return new Config(
    [
        "database" => [
            "adapter" => getenv('SQL_ADAPTER') ?: "postgresql",
            "host" => getenv('SQL_HOST') ?: "postgres",
            "port" => getenv('SQL_PORT') ?: "5432",
            "username" => getenv('SQL_USERNAME') ?: "root",
            "password" => getenv('SQL_PASSWORD') ?: "password",
            "dbname" => getenv('SQL_DBNAME') ?: "notes",
        ]
    ]
);