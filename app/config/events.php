<?php

use App\Middleware\ResponseMiddleware;

$app->finish(
    new ResponseMiddleware(
        $di->get('collectorRegistry')
    )
);