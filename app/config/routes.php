<?php

use App\Controller\MonitorController;
use App\Controller\MyObjectController;
use Phalcon\Mvc\Micro\Collection as MicroCollection;

return [
    function () {
        $controller = new MicroCollection();
        $controller->setHandler(new MonitorController());
        $controller->setPrefix('/actuator');
        $controller->get('/health', 'health');
        $controller->get('/metrics', 'metrics');
        return $controller;
    },
    function () {
        $controller = new MicroCollection();
        $controller->setHandler(new MyObjectController());
        $controller->setPrefix('/myobject');
        $controller->get('/{id}', 'get');
        return $controller;
    },
];