<?php


use App\Handler\ErrorHandler;
use Phalcon\Db\Adapter\Pdo\Factory;
use Phalcon\Di\FactoryDefault;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\Stream;
use Phalcon\Logger\Multiple;
use TweedeGolf\PrometheusClient\CollectorRegistry;
use TweedeGolf\PrometheusClient\Storage\ApcuAdapter;

$di = new FactoryDefault();

$di->set('config', $config);

$di->set(
    "logger",
    function () {
        $logger = new Multiple();

        $errorStream = new Stream("php://stderr");
        $errorStream->setLogLevel(Logger::ERROR);
        $logger->push($errorStream);

        $anyStream = new Stream("php://stdout");
        $anyStream->setLogLevel(Logger::DEBUG);
        $logger->push($anyStream);

        return $logger;
    }
);

$di->set(
    'db',
    function () use ($config) {
        return Factory::load($config->database);
    }
);

$di->set(
    "errorHandler",
    function () {
        return new ErrorHandler();
    }
);

$di->set(
    "collectorRegistry",
    function () {
        $registry = new CollectorRegistry(new ApcuAdapter());

        $registry->createCounter(
            'requests',
            ['route', 'method', 'status'],
            'HTTP server request summary',
            CollectorRegistry::DEFAULT_STORAGE,
            true
        );
        $registry->createHistogram(
            'response_timing',
            ['route', 'method', 'status'],
            ['0.005', '0.01', '0.02', '0.03', '0.05', '0.075', '0.1', '0.2', '0.3', '0.5', '0.75', '1.0', '2.5', '5.0', '7.5', '10.0'],
            'HTTP response time',
            CollectorRegistry::DEFAULT_STORAGE,
            true
        );

        return $registry;
    }
);

return $di;